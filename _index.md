---
title: OW2
themes:
 - Community advocacy
website: https://www.ow2.org
logo: stands/ow2_open_source_community/ow2-logo.svg
description: |
    Established in 2007 as a non-profit organisation, OW2 is a “community of communities”. Our mission is to make freely available a portfolio of open source software for enterprise information systems in domains such as middleware, open cloud, big data, BPM, security and privacy, software engineering, collaboration, etc. OW2 federates 50+ organizations and 2500+ IT professionals world wide. OW2 hosts 50+ technology Projects. Visit our stand and meet with OW2 Management Office staff and project leaders.

showcase: |
    <img src='/stands/ow2_open_source_community/ow2atFosdem22.jpg' class="img img-fluid" alt="Welcome to the OW2 stand"/>

    <h2>OW2 Initiatives showcased at FOSDEM</h2>

    <p><i>The virtual booth will also introduce two strategic initiatives recently launched by OW2: the analysis of open source project’s Market Readiness Levels, and the OW2 OSS Good Governance initiative, a framework for appropriate usage of open source software. These initiatives aim at contributing to the sustainability of the European open source ecosystem:</i></p>

    <h3>OW2 Market Readiness Levels</h3>

    <div style='width:200px'><img src="/stands/ow2_open_source_community/OW2_MRL_logo_RGB.svg" style='display:table;width:200px;margin:20px auto;' class="img img-fluid" alt="OW2 Market Readiness Levels"/></div>

    <p>Already applied to all 25 OW2 mature projects, the <a href="https://www.ow2.org/view/MRL/">Market Readiness Levels</a> scoring assesses the processes, key metrics and business resources of our open source projects and rates them on a market readiness scale similar to that of the NASA’s Technology Readiness Levels (TRL) rating. This synthetic approach has benefits for the developers as it provides them a blueprint for progressing, and for the users as it helps them make informed decision from a corporate rather than technical perspective. </p><div class="clearfix"><a class="btn btn-primary btn-sm float-right" href="https://www.ow2.org/view/MRL/" target="_blank">Learn More</a></div>

    <p></p><hr style="clear: both;"/><p></p>

    <h3>OW2 Good Governance Initiative</h3>
    <div style='width:200px'><img src="/stands/ow2_open_source_community/Logo_GoodGorvernanceInitiative_RGB.svg" style='display:table;width:200px;margin:20px auto;' class="img img-fluid" alt="OW2 Good Governance Initiative"/></div>
    <p>The <a href="https://www.ow2.org/view/OSS_Governance/"> OW2 OSS Good Governance initiative</a> addresses the needs of large end-users and digital service companies to better manage consumption of, and contribution to OSS. We will showcase the Open Source Good Governance Resource Center, a platform where EU users can discover, and a blueprint for implementing open source competency centers (aka OSPOs). The scope of the initiative goes beyond mere vulnerability and license compliance management: it aims to make C-level executive aware of the benefits of open source and of supporting the European open source ecosystem.
    </p> <div class="clearfix"><a class="btn btn-primary btn-sm float-right" href="https://www.ow2.org/view/OSS_Governance/" target="_blank">Learn More</a></div>
    <p></p><p></p>
    <h2>OW2 projects and code base</h2>
    <div style="background:#00A6D6;display:table;padding:20px;width:100%">
    <div style='display:table;margin:0 auto'><img src="/stands/ow2_open_source_community/Fasten_256x78.png" class="img img-fluid" alt="FASTEN Research Project" style='width:200px;float:left;margin-right:10px;margin-top:-5px' />
    <span style='font-size:1.3em;vertical-align:middle;font-family:"Signika"'>FASTEN project presented at the Devroom "Software Composition and Dependency Management"</span></div>
     </div>
    <p style='background:#F0F0F0;margin-top:0;padding:10px;border:2px solid #00A6D6'>FASTEN is a research project addressing dependencies issues by analyzing software packages at the finer-grain level and producing a large network of software ecosystems. The resulting dependency management functionality will be provided in famous package managers, such as Maven and PyPI. Join the presentation on Sunday at 15:00pm: <a href="https://fosdem.org/2022/schedule/track/software_composition_and_dependency_management/">FASTEN: Fine-Grained Analysis of Software Ecosystems as Networks</a>.  
    <div class="clearfix"><a class="btn btn-primary btn-sm float-right" href="https://www.fasten-project.eu" target="_blank">Learn More</a></div></p>


    <i>The <a href="https://projects.ow2.org/view/wiki/">OW2 code base</a> hosts dozens of open source projects such as ASM, AuthzForce, CLIF, DocDoku, FusionDirectory, GLPI, JORAM, Knowage, LemonLDAP:NG, Lutece, OCS Inventory, Petals ESB, Prelude, ProActive, Rocket.Chat, SAT4J, SeedStack, Sympa, Telosys, Waarp, WebLab and XWiki. In addition to the FASTEN project introduced above, the projects presented below will be showcased on our booth at FOSDEM. </i><div class="clearfix"><a class="btn btn-primary btn-sm float-right" href="https://projects.ow2.org/view/wiki/" target="_blank">Learn More</a></div>

    <p></p><p></p><hr style="clear: both;"/><p></p><p></p>

    <div style='width:200px'><img src="/stands/ow2_open_source_community/logo-ocs.svg" style='display:table;width:150px;margin:20px auto' class="img img-fluid" alt="OCS Inventory"/></div>
    <p><a href="https://projects.ow2.org/view/ocsinventory/">OCS Inventory</a> "Open Computers and Software Inventory" is an assets management and deployment solution. A project initiated by French "Gendarmerie Nationale" (a nationwide French police force) 20 years ago, OCSInventory is now a reliable mature product registered in the official catalog of recommended free software for the French public sector. With a Market Readiness Level at 9, it benefits from a position of established outsider in its market.</p>
    <div class="clearfix"><a class="btn btn-primary btn-sm float-right" href="https://projects.ow2.org/view/ocsinventory/" target="_blank">Learn More</a></div>
    <p></p><hr style="clear: both;"/><p></p>

    <div style='width:200px'><img src="/stands/ow2_open_source_community/REACHOUT_Logo.jpg" style='display:table;width:200px;margin:20px auto;' class="img img-fluid" alt="Reachout"/></div>
    <p><a href="https://www.reachout-project.eu/">ReachOut</a> offers a beta-testing campaign platform for research projects. Acting as as an operational intermediary between software developers and early adopters, ReachOut helps SMEs and research projects to implement their beta-testing campaign with appropriate software packaging, an efficient test scenario and a customised questionnaire. ReachOut is a support action funded by the European Commission under the Horizon 2020 programme. 
    <div class="clearfix"><a class="btn btn-primary btn-sm float-right" href="https://www.reachout-project.eu/" target="_blank">Learn More</a></div>
    <p></p><hr style="clear: both;"/><p></p>

    <div style='width:200px'><img src="/stands/ow2_open_source_community/DECODER_Logo.svg" style='display:table;width:100px;margin:20px auto' class="img img-fluid" alt="Decoder"/></div>
    <p><a href="https://www.decoder-project.eu">Decoder</a> provides a comprehensive open source framework for DevSecOps teams, leveraging NLP (Natural Language Processing) technologies, and cutting-edge AI/ML (Artificial Intelligence/Machine Learning) to provide greater efficiency throughout the application lifecycle, enhancing code understanding, readability, reuse, compliance and security. It automatically analyses and documents cloud and IoT projects, providing a deep understanding of software code and changes.</p> 
    <div class="clearfix"><a class="btn btn-primary btn-sm float-right" href="https://www.decoder-project.eu" target="_blank">Learn More</a></div>

    <p><a href="https://projects.ow2.org/view/wiki/">Explore the full OW2 code base</a>.</p> 

    <p></p><hr style="clear: both;"/><p></p>
chatroom: ow2
layout: stand
---
